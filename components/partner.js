import Image from 'next/image'
const Partner = () =>{
    return(
        <div>
            <div>
                <h1 className="title-main2">Our Partner</h1> 
            </div>
            <div>
                <Image
                src="/S__79798306.jpg"
                width={250}
                height={97.25}
                className="img-alight"/>
                
            </div>
        </div>
    )
}

export default Partner;
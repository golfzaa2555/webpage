import Image from 'next/image'
const Nav =()=>{

    return(

        <nav className="nav-bar">
            <div className="logo">
            <Image
                src="/Logo.psd"
                alt="Logo"
                width={100}
                height={100}
            />
            
            </div>
        </nav>
        
    )
}

export default Nav;


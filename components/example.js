import Image from 'next/image'
const Example = () =>{
    return(
        <div>
            <div>
                <h1 className="title-main2-2">ตัวอย่างการติดตั้ง</h1> 
            </div>
            <div>
                
                <Image
                src="/S__79798314.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1>Cafe</h1>
            </div>
            <div>
                
                <Image
                src="/S__79798312.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1 className="txt-righ">Classroom</h1>
            </div>
            <div>
                
                <Image
                src="/S__79798313.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1>Fitness</h1>
            </div>
            <div>
                
                <Image
                src="/S__79798310.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1 className="txt-righ">Office</h1>
            </div>
            <div>
                <Image
                src="/S__79798309.jpg"
                width={843}
                height={497}
                className="img-alight"/>
            </div>
            <div>
                <Image
                src="/S__79798308.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1>คลินิกทันตกรรม โรงพยาบาลอู่ทอง</h1>
            </div>
            <div>
                <h1 className="title-main2-2">Fresh Air Solution</h1>
                <Image
                src="/S__79798305.jpg"
                width={843}
                height={497}
                className="img-alight"/>
            </div>

        </div>
    )
}

export default Example;
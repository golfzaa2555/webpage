import Image from 'next/image'
const Details = () =>{
    return(
        <div>
            <div className="title-main">
                <h1>Fresh Air Solution</h1>
                <h4>อากาศสะอาดที่คุณสร้างเองได้</h4>
            </div>
            <div>
            <h1 className="title-main2">HEPA CIRCULATION UNIT: HCU</h1>  
                <h4 className="title-main3">เครื่องกรองอากาศควบคุมเชื้อ</h4>
                <h5 className="title-main4">สะอาดปลอดภัยหายใจได้เต็มปอด</h5>
                <div className="line"></div>
                <br/>

                <Image
                src="/S__79798302.jpg"
                width={843}
                height={497}
                className="img-alight"/>
            </div>
            <div>
                <h1>เครื่องกรองอากาศควบคุมเชื้อ</h1>
                <Image
                src="/S__79822850.jpg"
                width={180}
                height={409}
                className="img-alight"/>
                
            </div>
            <div>
                <Image
                src="/S__79798303.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <h1>Fresh Air Unit เติมอากาศสะอาดในบ้านเพื่อคนที่คุณรัก</h1>
                <Image
                src="/S__79798304.jpg"
                width={843}
                height={497}
                className="img-alight"/>

            </div>
            <div>
                <h1 className="title-main2">เติมอากาศสะอาดผ่าน Pre Filter และ HEPA Filter</h1>
                <div>
                    <p>・ HEPA FILTER สามารถกรองอนุภาคขนาดเล็กได้ถึง 0.3 ไมครอน ที่ประสิทธิภาพ 99.99%</p>
                    <p>・ สามารถป้องกัน PM 2.5 และมลพิษจากภายนอกได้</p>
                    <p>・ สามารถป้องกันการปนเปื้อนของเชื้อไวรัส โควิด-19 จากภายนอกได้</p>
                </div>
                <Image
                src="/S__79798301.jpg"
                width={843}
                height={497}
                className="img-alight"/>
                <p className="txt-center">4.5 ACH ที่ 16 ตร.ม. - 1 ACH ที่ 64 ตร.ม. ที่ความสูงเพดาน 2.7 ม.</p>
                
            </div>
            <div>
                <h1 className="title-main2-2">ปกป้องห้องสำคัญด้วย Positive Pressure</h1>
                <Image
                src="/S__79798315.jpg"
                width={843}
                height={497}
                className="img-alight"/>    
                <p className="txt-size"><span className="txt-blod">หลักการ positive pressure</span> คือ การทำให้ภายในห้องมีความดันอากาศมากกว่าภายนอกห้อง ความดันในห้องที่มากกว่าจะป้องกันการรั่วไหลของของอากาศ</p>
                <p className="txt-size">ภายนอกผ่านรอยรั่วของห้อง เช่น ร่องประตู,ขอบหน้าต่าง,ขอบฝ้าเพดาน และรอยร้าวของผนัง เป็นต้น</p>       
            </div>
            <div>
                <h1>HEPA Circulation Unit</h1>
                <p className="txt-size">
                    ระบบสร้างอากาศบริสุทธิ์สำหรับทันตกรรม การออกแบบสอดคล้องกับคำแนะนำตามมาตรฐานกองแบบแผน กระทรวงสาธารณสุข ตามเอกสารเลขที่ 
                </p>
                <p className="txt-size">
                ก.45/เม.ย./63 โดยมุ่งเน้นไปที่ส่วนสำคัญ 2 ส่วนหลักคือ
                </p>
                <p className="txt-size">
                    1. เติมอากาศบริสุทธิ์จากภายนอก ซึ่งผ่านการกรองด้วย 
                    <span className="span_1">HEPA Filter class H13</span>เข้ามาภายในห้อง
                    <span className="span_1">3 ACH</span>
                </p>
                <p className="txt-size">2. หมุนเวียนอากาศภายในห้องให้บริสุทธิ์ โดยกรองผ่าน
                    <span className="span_1">HEPA Filter class H14 : EN1822 (2009)</span>เมื่อขณะทำงาน ระบบในห้องสามารถสร้างอากาศสะอาดอยู่ที่
                    <span className="span_1">21</span> 
                    <p>ACH และเมื่อเสร็จงาน การทำงานช่วงพักห้องระบบสามารถสร้างอากาศสะอาดสูงสุดที่
                    <span className="span_1">50 ACH</span>ด้วยการออกแบบระบบนี้ จึงสามารถทำความสะอาดในห้องให้สะอาด
                    </p>
                    
                    <span className="span_1">99.9%</span> ภายในระยะเวลา 
                    <span className="span_1">8
                    </span>นาที ซึ่งลดระยะเวลาการพักห้อง และสามารถรับเคสต่อไปได้เร็วขึ้น
                </p>
                
                <div>
                    <p>8 MINS FOR REMOVAL WITH 99.9% EFFICIENCY</p>
                    <p>(50 ACH @ standard Room size 3(w)x4(l)x2.8(h))</p>
                </div>
                
                <Image
                src="/S__79798351.gif"
                width={843}
                height={497}
                className="img-alight"/>
            </div>

        </div>
    )
}

export default Details;
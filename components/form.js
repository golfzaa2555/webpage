import {useState} from 'react'

const Form = () =>{

    const initialState = { name:'',phone:'',email:'',message:''}  
    const [userData,setUserData] = useState(initialState) 
    const {name,phone,email,message} = userData
    const contentType = 'application/json'
    const handleChangeInput = e =>{
        const {name, value} = e.target
        setUserData({...userData, [name]:value})
    }

   
  const postData = async (userData) => {
    try {
      const res = await {
        method: 'POST',
        headers: {
          Accept: contentType,
          'Content-Type': contentType,
        },
        body: JSON.stringify(userData),
      }

     
      if (!res.ok) {
        throw new Error(res.status)
      }

      
    } catch (error) {
      console.log('Failed to add')
     
    }
  }

    const handleSubmit = e =>{
        e.preventDefault()
        console.log(userData)
        postData(userData)
    }
    return(
        <div>
            <div><h1>Contact Us</h1></div>
            <div>
                <form onSubmit={handleSubmit}> 
                    <div>
                        <label htmlFor="name" className="lable1">Name</label>
                        <div>
                        <input className="inputform" type="text"  id="name"  name="name"  value={name} onChange={handleChangeInput} placeholder="Name"  />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="phone" className="lable1">Phone number</label>
                        <div>
                        <input className="inputform" type="text"  id="phone"  name ="phone" value={phone} onChange={handleChangeInput} placeholder="Phone number"  />
                        </div>
                    </div>
                    <div>
                        <label htmlFor="email" className="lable1">Email</label>
                        <div>
                        <input className="inputform" type="text"  id="email"  name ="email" value={email} onChange={handleChangeInput} placeholder="Email"  />
                        </div>
                    </div>
                    <div>
                    <label htmlFor="message" className="lable1">Message</label>
                        <div>
                        <input className="inputform" type="text"  id="message"  name ="message" value={message} onChange={handleChangeInput} placeholder="Message"  />
                        </div>
                    </div>
                    <br/>
                    <button type="submit" className="submit-btn">Send</button>
                </form>
            </div>
        </div>
    )
}

export default Form;
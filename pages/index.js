import React from 'react'
import Head from 'next/head'
import Form from '../components/form'
import Details from '../components/details'
import Example from '../components/example'
import Nav from '../components/nav'
import Line from '../components/buttonline'
import Clients from '../components/clients'
import Partner from '../components/partner'




const Index =()=>{

    return(

        <div className="Home">
            <Head>
           
            <title>เครื่องกรองอากาศควบคุมเชื้อ เติมอากาศสะอาด - tectony</title>
            <link rel="icon" href="/Logo.ico"></link>
            </Head>
            <Nav></Nav>
            <Details></Details>
            <Example></Example>
            <Partner></Partner>
            <Clients></Clients>
            <Line></Line>
            <Form></Form>
        </div>
        
    )
}

export default Index;